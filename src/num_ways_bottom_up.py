from typing import Set


def num_ways(stairs: int, steps: Set):
    if stairs == 0:
        return 1
    nums = [1] + [0 for _ in range(stairs)]
    for i in range(1, stairs+1):
        total = 0
        for way in steps:
            if i - way >= 0:
                total += nums[i - way]
        nums[i] = total
    return nums[stairs]
