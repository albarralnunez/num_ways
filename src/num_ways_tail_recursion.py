from copy import copy
from typing import Set


def _num_ways(stairs: int, steps: set, next_steps: set, cont):
    if stairs == 0:
        return lambda: cont(1)
    if stairs < 0 or not next_steps:
        return lambda: cont(0)
    way = next_steps.pop()
    return lambda: _num_ways(
        stairs,
        steps,
        next_steps,
        lambda value: _num_ways(
            stairs - way,
            steps,
            copy(steps),
            lambda value2: lambda: cont(value + value2)
        ))


def trampoline(f, *args):
    v = f(*args)
    while callable(v):
        v = v()
    return v


def num_ways(stairs: int, steps: Set):
    return trampoline(
        _num_ways, stairs, steps, copy(steps),
        lambda value: value
    )
