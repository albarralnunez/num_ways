from typing import Set


def num_ways(stairs: int, steps: Set):
    if stairs == 0:
        return 1
    if stairs < 0:
        return 0
    return sum(map(lambda way: num_ways(stairs - way, steps), steps))
