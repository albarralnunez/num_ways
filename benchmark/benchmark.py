import pandas as pd
import numpy as np
import timeit


def benchmark(inputs, functions, executions, repetitions):
    d = {}
    for name, func in functions.items():
        row = []
        for kwargs in inputs:
            executions_time = 0
            try:
                executions_time = timeit.repeat(
                    number=executions, repeat=repetitions,
                    stmt=f"num_ways(**{kwargs})",
                    setup=func
                )
            except RecursionError:
                pass
            finally:
                row.append(np.mean(executions_time))
        d.update({name: row})
    return pd.DataFrame(data=d, index=[str(x) for x in inputs])
