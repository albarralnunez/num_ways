FROM jupyter/minimal-notebook

COPY requirements /tmp/requirements
RUN pip install -r /tmp/requirements/base.txt && \
    pip install -r /tmp/requirements/local.txt

RUN mkdir workspace
WORKDIR workspace