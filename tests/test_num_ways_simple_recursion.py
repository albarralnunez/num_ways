import pytest

from src.num_ways_recursion import num_ways


@pytest.mark.parametrize("test_input,expected", [
    ({'stairs': 1, 'steps': {1, 2}}, 1),
    ({'stairs': 2, 'steps': {1, 2}}, 2),
    ({'stairs': 4, 'steps': {1, 2}}, 5),
    ({'stairs': 3, 'steps': {1, 2}}, 3),
    ({'stairs': 4, 'steps': {1, 3, 5}}, 3),
])
def test_num_ways_success(test_input, expected):
    assert(expected == num_ways(**test_input))


def test_num_ways_recursion_error():
    with pytest.raises(RecursionError):
        num_ways(stairs=400, steps={1})
