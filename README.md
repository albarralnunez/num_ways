
# Development instructions

Create git hook for ensuring code quality
``` bash
flake8 --install-hook git
git config --bool flake8.strict true
```
